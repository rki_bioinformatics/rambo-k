package org.rki.readclassifier;

public class KmerDistribution {
	public double mean;
	public double sd;
	public double zeroval;
	
	private double factor;
	
	public KmerDistribution(double mean, double sd) {
		this.mean = mean;
		this.sd = sd;
		factor = 1.0/(sd*Math.sqrt(2*Math.PI));
		zeroval = valueAt(0);
	}

	public double valueAt(double x) {
		return x == 0?zeroval:factor*Math.exp(-1*(x-mean)*(x-mean)/(2*sd*sd));
	}
}
