package org.rki.readclassifier;

public class ReadPair {
	public Read read1;
	public Read read2;
	
	public ReadPair(Read read1, Read read2) {
		this.read1 = read1;
		this.read2 = read2;
	}
}
